package eu.learn.yla.firstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnClickMe = findViewById<Button>(R.id.mybutton)
        val textView = findViewById<TextView>(R.id.mytext)
        var timesClicked = 0
        btnClickMe.setOnClickListener {
            textView.text = "${++timesClicked}"
            Toast.makeText(this, "mytext was clicked ${timesClicked} times", Toast.LENGTH_LONG).show()
        }

    }
}